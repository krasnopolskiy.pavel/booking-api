# Booking APP RESTFull API
Link for testing: http://54.165.69.133:5000/api/
## Docs
### Resource list:
* **hotel**
  * 'name': StrField,
  * 'city': ReferenceField,
  * 'address': StrField,
  * 'location': GeoPointField, in format "54.12341,63.12312"
  * 'stars': NumericField,
  * 'photos': StrField, path to photos
* **room**
  * 'status': StrField,
  * 'name': StrField,
  * 'default_price': NumericField,
  * 'photos': StrField,
  * 'capacity': NumericField,
* **booking**
  * 'date': DateField,
  * 'is_booked': BoolField,

### Hotel resource
**Availiable paths**<br>
/api/hotel<br>
/api/hotel/id
#### GET
Returns an instance of hotel<br>
**Parameters**:<br>
* mode: str, Any of <b>'single' 'multiple'</b>
* id: str, if mode==multiple, coma separated
#### POST
Creates an instance of hotel<br>
**Parameters**:<br>
all fields in declared format
#### DELETE
Deletes instance of hotel<br>
**Parameters**:<br>
* id: str
#### PUT
Modifies an instance of hotel<br>
**Parameters**:<br>
* any fields in declared format<br>
* id: str

### Room resource
**Availiable paths**<br>

'/api/room' <br>
'/api/hotel/string:id_hotel/room' <br>
'/api/hotel/string:id_hotel/room/string:id <br>
#### GET
Returns an instance of room<br>
**Parameters**:<br>
* mode: str, Any of <b>'single' 'multiple'</b>
* id: str, if mode==multiple, coma separated
* id_hotel: str
#### POST
Creates an instance of hotel<br>
**Parameters**:<br>
* all fields in declared format
* id_hotel: str
#### DELETE
Deletes instance of hotel<br>
**Parameters**:<br>
* id: str
* id_hotel: str
#### PUT
Modifies an instance of hotel<br>
**Parameters**:<br>
* any fields in declared format<br>
* id: str
* id_hotel: str


### Booking resource
**Availiable paths**<br>

'/api/booking' <br>
'/api/hotel/string:id_hotel/room/string:id_room/booking' <br>

#### GET
Returns free or booked dates for certain room<br>
**Parameters**:<br>
* mode: str, Any of <b>'free_dates', 'booked_dates'</b>
* from: Date, in format 20220101
* to: Date, in format 20220101
* id_hotel: str
* id_room: str
#### POST
Books dates for a room<br>
**Parameters**:<br>
* from: Date, in format 20220101
* to: Date, in format 20220101
* id_hotel: str
* id_room: str
#### DELETE
Deletes booking from dates<br>
**Parameters**:<br>
* from: Date, in format 20220101
* to: Date, in format 20220101
* id_hotel: str
* id_room: str
