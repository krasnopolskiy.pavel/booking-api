import datetime
import os
from abc import ABC
from collections.abc import Iterable

import pytz
from google.cloud.firestore import GeoPoint, DocumentReference, Client

db = Client.from_service_account_json(os.environ.get('GOOGLE_CREDS_PATH'))


class BaseField(ABC):
    python_type = str
    db_datatype = str

    def __init__(self, value=None):
        self.value = value if value is None else self.validate(value)

    def validate(self, value):
        if isinstance(value, self.db_datatype):
            return self.from_db_datatype(value)
        else:
            try:
                return self.convert(value, self.python_type)
            except TypeError:
                raise TypeError(f'{value} should be a {self.python_type} object')

    def to_dict(self):
        return self.value

    def __repr__(self):
        return str(self.value)

    def to_db_datatype(self, value=None, db=db):
        if value is None:
            if self.value is None:
                return None
            value = self.value
        return self.convert(value, self.db_datatype)

    def from_db_datatype(self, value=None):
        return self.convert(value, self.python_type)

    @staticmethod
    def convert(value, to):
        if isinstance(to, Iterable):
            for type_ in to:
                try:
                    return type_(value)
                except TypeError:
                    pass
        elif isinstance(value, to):
            return value
        return to(value)


class StrField(BaseField):
    python_type = str
    db_datatype = str


class NumericField(BaseField):
    python_type = (int, float)
    db_datatype = (int, float)


class ReferenceField(BaseField):
    python_type = str
    db_datatype = DocumentReference

    def from_db_datatype(self, value=None):
        return value.path

    def to_db_datatype(self, value=None, db=db):
        if value is None:
            if self.value is None:
                return None
            value = self.value
        return self.db_datatype(*value.split('/'), client=db)

    def get(self, db=db):
        return self.to_db_datatype(db=db).get()


class GeoPointField(BaseField):
    db_datatype = GeoPoint
    python_type = str

    def validate(self, value):
        if isinstance(value, (tuple, list)):
            lat = value[0]
            lon = value[1]
        elif isinstance(value, dict):
            lat = value['latitude']
            lon = value['latitude']
        elif isinstance(value, str):
            lat, lon = value.split(',')
            lat, lon = float(lat), float(lon)
        elif isinstance(value, self.db_datatype):
            d = self.from_db_datatype(value)
            lat = d['latitude']
            lon = d['longitude']
        else:
            raise TypeError(f'Cannot parse GeoPoint from {value}')
        if lat < -90 or lat > 90:
            raise TypeError(f'latitude should be in [-90;90], got {lat}')
        if lon < -180 or lon > 180:
            raise TypeError(f'longitude should be in [-180;180], got {lon}')
        return {
            'latitude': lat,
            'longitude': lon
        }

    def to_db_datatype(self, value=None, db=db):
        if value is None:
            if self.value is None:
                return None
            value = self.value
        return self.db_datatype(**value)

    def from_db_datatype(self, value=None):
        if value is None:
            value = self.value
        return {
            'latitude': value.latitude,
            'longitude': value.longitude
        }


class DateField(BaseField):
    python_type = datetime.datetime
    db_datatype = datetime.datetime

    def from_db_datatype(self, value=None):
        return datetime.datetime.fromtimestamp(int(value.timestamp())).replace(tzinfo=pytz.UTC)

    def validate(self, value):
        if isinstance(value, self.python_type):
            return value
        elif isinstance(value, self.db_datatype):
            return self.from_db_datatype(value)
        elif isinstance(value, str):
            return self.python_type.strptime(value, '%Y%m%d').replace(tzinfo=pytz.UTC)
        else:
            raise TypeError


    def to_dict(self):
        return self.value.strftime("%Y-%m-%d")


class BoolField(BaseField):
    python_type = bool
    db_datatype = bool
