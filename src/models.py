from src.base_model import BaseModel
from src.fields import StrField, NumericField, ReferenceField, GeoPointField, DateField, BoolField


class City(BaseModel):
    collection_path = 'city'
    fields = {
        'name': StrField,
        'state': StrField,
        'country': StrField,
        'location': GeoPointField
    }


class Booking(BaseModel):
    collection_path_rule = 'hotel/%s/room/%s/booking'
    fields = {
        'date': DateField,
        'is_booked': BoolField,
    }


class Room(BaseModel):
    collection_path_rule = 'hotel/%s/room'
    fields = {
        'status': StrField,
        'name': StrField,
        'default_price': NumericField,
        'photos': StrField,
        'capacity': NumericField,
    }
    collections = {
        'booking': Booking
    }


class Hotel(BaseModel):
    collection_path = 'hotel'
    fields = {
        'name': StrField,
        'city': ReferenceField,
        'address': StrField,
        'location': GeoPointField,
        'stars': NumericField,
        'photos': StrField
    }
    collections = {
        'room_type': Room
    }
