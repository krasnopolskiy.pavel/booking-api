from flask_restful import abort
from flask_restful import reqparse

from src.default_resourse import DefaultResource
from src.model_reqparser import ModelReqparser
from src.models import Room


class RoomResource(ModelReqparser, DefaultResource):
    model = Room

    def create_non_default_reqparse_arguments(self):
        return {
            'mode': reqparse.Argument(
                name='mode', type=str, default='single',
                location='args', required=False, store_missing=True,
                choices=['single', 'multiple', 'query']
            ),
            'id_hotel': reqparse.Argument(
                name='id_hotel', type=str,
                location=['args', 'view_args'], required=True
            )
        }

    def get(self, **kwargs):
        parser = self.create_parser('mode', 'id', 'depth', 'id_hotel')
        args = parser.parse_args()
        response = self.get_split(args)
        return self.format_get_response(args, response)

    def get_single(self, args):
        o = self.model(id_=args['id'], collection_path=(args['id_hotel'],)).get()
        return o.to_dict()

    def get_query(self, args):
        return abort(501)

    def get_multiple(self, args):
        args_single = args.copy()
        result_list = []
        for key in args.get('id').split(','):
            args_single['id'] = key
            result_list.append(self.get_single(args_single))
        return result_list

    def post(self, **kwargs):
        args = self.create_parser(
            'capacity', 'name', 'default_price',
            'id_hotel',
            photos={'required': False}
        ).parse_args()
        o = self.model(collection_path=(args['id_hotel'],), **args).add()
        return self.format_post_response(args, o)

    def delete(self, **kwargs):
        args = self.create_parser('id_hotel', 'id').parse_args()
        o = self.model(collection_path=(args['id_hotel'],), id_=args['id']).delete()
        return self.format_delete_response(args, o)

    def put(self, **kwargs):
        args = self.create_parser(
            'id', 'id_hotel',
            name={'required': False},
            capacity={'required': False},
            default_price={'required': False},
            photos={'required': False}
        ).parse_args()
        o = self.model(id_=args.get('id'), collection_path=(args['id_hotel'],), **args).update()
        return self.format_put_response(args, o)
