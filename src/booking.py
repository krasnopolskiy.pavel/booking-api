import datetime

import pytz
from flask import jsonify
from flask_restful import Resource, abort
from flask_restful import reqparse

from src.fields import DateField
from src.model_reqparser import ModelReqparser
from src.models import Booking


class BookingResource(ModelReqparser, Resource):
    model = Booking

    def __init__(self, *args, **kwargs):
        self.today = datetime.datetime.fromordinal(datetime.date.today().toordinal()).replace(tzinfo=pytz.UTC)
        self.max_to = self.today + datetime.timedelta(days=30)
        self.min_from = self.today
        self.date_format_str = "%Y-%m-%d"
        super().__init__(*args, **kwargs)

    def create_non_default_reqparse_arguments(self):
        return {
            'mode': reqparse.Argument(
                name='mode', type=str, default='free_dates',
                location='args', required=False, store_missing=True,
                choices=['free_dates', 'booked_dates']
            ),
            'from': reqparse.Argument(
                name='from', type=DateField().validate,
                default=self.today,
                location='args', required=True, store_missing=True
            ),
            'to': reqparse.Argument(
                name='to', type=DateField().validate,
                default=self.today + datetime.timedelta(days=7),
                location='args', required=True, store_missing=True
            ),
            'id_hotel': reqparse.Argument(
                name='id_hotel', type=str,
                location='args', required=True
            ),
            'id_room': reqparse.Argument(
                name='id_room', type=str,
                location='args', required=True
            )
        }

    def get(self, **kwargs):
        args = self.create_parser(
            'mode', 'id_hotel', 'id_room', 'from', 'to') \
            .parse_args()
        if args['from'] > args['to']:
            abort(400, description="'from' must be before 'to'")
        args['from'] = max(args['from'], self.min_from)
        args['to'] = min(args['to'], self.max_to)
        response = self.get_split(args)
        return self.format_get_response(args, response)

    def get_split(self, args):
        if args['mode'] == 'free_dates':
            response = self.get_booked_dates(args)
            response = self.format_get_free_results(args, response)
        elif args['mode'] == 'booked_dates':
            response = self.get_booked_dates(args)
            response = self.format_get_booked_results(args, response)
        return response

    def get_booked_dates(self, args):
        db = self.model.db
        booking_path = self.model().collection_path_resolver((args['id_hotel'], args['id_room']))
        results = db.collection(booking_path) \
            .where('date', '>=', args['from']) \
            .where('date', '<=', args['to']) \
            .where('is_booked', '==', True) \
            .get()
        results = [self.model(document=result).date.value for result in results]
        return results

    def format_get_free_results(self, args, results):
        intervals = []
        interval_start = args['from']
        interval_end = None
        for result in sorted(results):
            if result == interval_start:
                interval_start = interval_start + datetime.timedelta(days=1)
                continue
            interval_end = result
            intervals.append((interval_start, interval_end))
            interval_start = result + datetime.timedelta(days=1)
        if interval_start < args['to']:
            intervals.append((interval_start, args['to']))
        intervals = [{
            'from': interval_start.strftime(self.date_format_str),
            'to': interval_end.strftime(self.date_format_str)
        } for interval_start, interval_end in intervals]
        return intervals

    def format_get_booked_results(self, args, results):
        intervals = []
        interval_start = min(results)
        interval_end = args['from']
        for result in sorted(results):
            if result == interval_end:
                interval_end = interval_end + datetime.timedelta(days=1)
                continue
            intervals.append((interval_start, result))
            interval_start = result + datetime.timedelta(days=1)

        if interval_end == args['to']:
            intervals.append((interval_start, interval_end))
        intervals = [{
            'from': interval_start.strftime(self.date_format_str),
            'to': interval_end.strftime(self.date_format_str)
        } for interval_start, interval_end in intervals]
        return intervals

    def format_get_response(self, args, obj):
        return jsonify({
            'request': {
                'method': 'GET',
                'params': args
            },
            'result': {
                'status': 'OK',
                'dates': obj
            }
        })

    def post(self, **kwargs):
        args = self.create_parser(
            'from', 'to', 'id_room',
            'id_hotel'
        ).parse_args()
        booked_dates = self.get_booked_dates(args)
        is_booked_dates_inside_range = [(d > args['from'] and d < args['to']) for d in booked_dates]
        if any(is_booked_dates_inside_range):
            abort(409, message='Cannot book dates. They are already booked')
        date_ = args['from']
        while date_ < args['to']:
            o = self.model(
                collection_path=(args['id_hotel'], args['id_room']),
                date=date_, is_booked=True).add()
            date_ = date_ + datetime.timedelta(days=1)
        return self.format_post_response(args)

    def format_post_response(self, args):
        return jsonify({
            'request': {
                'method': 'POST',
                'params': args
            },
            'result': {
                'status': 'OK'
            }
        })

    def delete(self, **kwargs):
        args = self.create_parser('id_hotel', 'id_room', 'from', 'to').parse_args()
        db = self.model.db
        booking_path = self.model().collection_path_resolver((args['id_hotel'], args['id_room']))
        results = db.collection(booking_path) \
            .where('date', '>=', args['from']) \
            .where('date', '<=', args['to']) \
            .get()
        for result in results:
            self.model(document=result).delete()
        return self.format_delete_response(args, 'OK')

    def put(self):
        abort(501, message="Not implemented")

    def format_delete_response(self, args, status='OK'):
        return jsonify({
            'request': {
                'method': 'DELETE',
                'params': args
            },
            'result': {
                'status': status
            }
        })

    def format_put_response(self, args, status='OK'):
        return jsonify({
            'request': {
                'method': 'PUT',
                'params': args
            },
            'result': {
                'status': status
            }
        })
