import copy

from flask_restful import reqparse


class ModelReqparser:
    model = None

    def __init__(self, *args, **kwargs):
        self.reqparse_arguments = self.create_default_reqparse_arguments()
        self.reqparse_arguments.update(self.create_non_default_reqparse_arguments())
        super().__init__(*args, **kwargs)

    def create_parser(self, *args, **kwargs):
        parser = reqparse.RequestParser()
        for arg in args:
            if isinstance(arg, str):
                parser.add_argument(self.reqparse_arguments[arg])
            elif isinstance(arg, reqparse.Argument):
                parser.add_argument(arg)
        for arg_name, arg_params in kwargs.items():
            arg = copy.deepcopy(self.reqparse_arguments[arg_name])
            for k, v in arg_params.items():
                setattr(arg, k, v)
            parser.add_argument(arg)
        return parser

    def create_default_reqparse_arguments(self):
        d = {
            field_name: reqparse.Argument(
                name=field_name,
                type=field_class().validate,
                required=True,
                store_missing=False,
                location='args',
                help=f'Smth wrong with {field_name} param'
            ) for field_name, field_class in self.model.fields.items()
        }
        d['id'] = reqparse.Argument(
            name='id', type=str,
            location=['args', 'view_args'], required=True,
            help=f'Smth wrong with id param'
        )
        d['depth'] = reqparse.Argument(
            name='depth', type=int,
            location='args', required=False,
            default=0,
            choices=[-1, 0, 1, 2, 3],
            help=f'Smth wrong with depth param'
        )
        return d

    def create_non_default_reqparse_arguments(self):
        return {}
