from abc import ABC, abstractmethod

from flask import jsonify
from flask_restful import abort, Resource


class DefaultResource(ABC, Resource):
    def get_split(self, args):
        if args['mode'] == 'single':
            response = self.get_single(args)
        elif args['mode'] == 'multiple':
            response = self.get_multiple(args)
        elif args['mode'] == 'query':
            response = self.get_query(args)
        else:
            abort(400)
        return response

    @abstractmethod
    def get_single(self, args):
        pass

    def get_multiple(self, args):
        args_single = args.copy()
        result_list = []
        for key in args.get('id').split(','):
            args_single['id'] = key
            result_list.append(self.get_single(args_single))
        return result_list

    def get_query(self, args):
        return abort(501)

    def format_get_response(self, args, obj):
        return jsonify({
            'request': {
                'method': 'GET',
                'id': args.id
            },
            'result': {
                'status': 'OK',
                'data': obj
            }
        })

    def format_post_response(self, args, obj):
        return jsonify({
            'request': {
                'method': 'POST',
                'params': args
            },
            'result': {
                'status': 'OK',
                'id': obj.id,
                'path': obj.collection_path
            }
        })

    def format_delete_response(self, args, status='OK'):
        return jsonify({
            'request': {
                'method': 'DELETE',
                'id': args.id
            },
            'result': {
                'status': status
            }
        })

    def format_put_response(self, args, status='OK'):
        return jsonify({
            'request': {
                'method': 'PUT',
                'params': args
            },
            'result': {
                'status': status
            }
        })
