import json
from abc import ABC

from flask_restful import abort
from google.api_core import exceptions

from src.fields import db


class BaseModel(ABC, object):
    db = db
    collection_path = None
    collection_path_rule = None
    fields = {}
    collections = {}

    def __init__(self, document=None, id_=None, collection_path=None, **kwargs):
        self.value = {}
        self.collection_value = {}
        self.id = None
        if id_:
            self.id = id_
        if collection_path:
            self.collection_path_resolver(collection_path)
        if document:  ## TODO: change for isinstance(document, doc_class)
            self.document = document
            self.id = document.id
            self.collection_path = '/'.join(document.reference.parent._path)
            self.from_dict_datatype(**document.to_dict())
        else:
            self.from_dict_datatype(**kwargs)

    def collection_path_resolver(self, collection_path):
        if self.collection_path_rule is None:
            self.collection_path = str(collection_path)
        else:
            self.collection_path = self.collection_path_rule % collection_path
        return self.collection_path

    def from_dict_datatype(self, **kwargs):
        self.value = {}

        for field_name, field_type in self.fields.items():
            inp_value = kwargs.get(field_name, None)
            if isinstance(field_type, (list, tuple)):
                self.value[field_name] = [field(inp_value) for field in field_type]
            elif isinstance(field_type, dict):
                self.value[field_name] = {k: v(inp_value) for k, v in field_type.item()}
            else:
                self.value[field_name] = field_type(inp_value)

    def get_collections(self, collection_list=None):
        if collection_list:
            collection_dict = {k: self.collections[k] for k in collection_list}
        else:
            collection_dict = self.collections
        for col_name, col_class in collection_dict.items():
            col_items = db.collection(f'{self.collection_path}/{self.id}/{col_name}').list_documents()
            col_items = list(col_items)
            col_items = [col_class(col_item.get()) for col_item in col_items]
            self.collection_value[col_name] = col_items
        return self.collections

    def __setattr__(self, key, value):
        if key in self.fields:
            self.value[key] = value
        else:
            super().__setattr__(key, value)

    def __getattr__(self, item):
        if item in self.fields:
            return self.value[item]

    def _call_in_depth_method(self, dict_object, method_name, *args, **kwargs):
        d = {}
        for key, value in dict_object.items():
            if isinstance(value, (tuple, list)):
                d[key] = [getattr(v, method_name)(*args, **kwargs) for v in value]
            elif isinstance(value, dict):
                d[key] = {k: getattr(v, method_name)(*args, **kwargs) for k, v in value.items()}
            else:
                d[key] = getattr(value, method_name)(*args, **kwargs)
        return d

    def to_db_datatype(self, value=None, db=None):
        if value is None:
            value = self.value
        result_dict = self._call_in_depth_method(value, 'to_db_datatype', db=db)
        result_dict = {k: v for k, v in result_dict.items() if v is not None}
        return result_dict

    def get(self):
        if self.id:
            doc = db.collection(self.collection_path).document(self.id).get()
            if doc.exists:
                self.__init__(document=doc)
                return self
            else:
                abort(404)
        abort(500)

    def add(self):
        values = self.to_db_datatype(db=db)
        update_time, obj_ref = db.collection(self.collection_path).add(values)
        self.id = obj_ref.id
        return self

    def set(self):
        values = self.to_db_datatype(db=db)
        document_ref = db.collection(self.collection_path).document(self.id)
        try:
            document_ref.set(values)
            return 'OK'
        except exceptions.NotFound:
            return 'document not found'

    def update(self):
        values = self.to_db_datatype(db=db)
        document_ref = db.collection(self.collection_path).document(self.id)
        try:
            document_ref.update(values)
            return 'OK'
        except exceptions.NotFound:
            return 'document not found'

    def delete(self):
        document_ref = db.collection(self.collection_path).document(self.id)
        try:
            document_ref.delete()
            return 'OK'
        except exceptions.NotFound:
            return 'document not found'

    def from_db_datatype(self, value=None):
        result_dict = self._call_in_depth_method(self.value, 'from_db_datatype')
        return result_dict

    def to_dict(self):
        from_fields = {
            'fields': self._call_in_depth_method(self.value, 'to_dict')
        }
        if self.id:
            from_fields.update({'id': self.id})
        if self.collection_value:
            from_collections = self._call_in_depth_method(self.collection_value, 'to_dict')
            from_fields.update({'collections': from_collections})
        return from_fields

    def __repr__(self):
        return json.dumps(self.to_dict(), indent=2)
