from flask_restful import abort, request
from flask_restful import reqparse
from flask_restful.inputs import boolean

from src.default_resourse import DefaultResource
from src.model_reqparser import ModelReqparser
from src.models import City, Hotel


class HotelResource(ModelReqparser, DefaultResource):
    model = Hotel

    def create_non_default_reqparse_arguments(self):
        return {
            'mode': reqparse.Argument(
                name='mode', type=str, default='single',
                location='args', required=False, store_missing=True,
                choices=['single', 'multiple', 'query']
            ),
            'room_type': reqparse.Argument(
                name='roomtype', type=boolean,
                location='args', required=False,
                default=False
            ),
            'room': reqparse.Argument(
                name='room', type=boolean,
                location='args', required=False,
                default=False
            )
        }

    def get(self, **kwargs):
        args = self.create_parser('mode', 'id', 'room_type', 'room').parse_args()
        response = self.get_split(args)
        return self.format_get_response(args, response)

    def get_single(self, args):
        h = self.model(id_=args['id']).get()
        h.city = City(document=h.city.get())
        if args.get('roomtype', None):
            h.get_collections()
        if args.get('room', None):
            for room_type in h.collection_value['room_type']:
                room_type.get_collections()
        return h.to_dict()

    def get_query(self, args):
        return abort(501)

    def post(self):
        args = self.create_parser(
            'name', 'city', 'address',
            'location', 'stars'
        ).parse_args()
        h = self.model(**args).add()
        return self.format_post_response(args, h)

    def delete(self, **kwargs):
        args = self.create_parser('id').parse_args()
        h = self.model(id_=args['id']).delete()
        return self.format_delete_response(args, h)

    def put(self, **kwargs):
        args = self.create_parser(
            'id',
            name={'required': False},
            city={'required': False},
            address={'required': False},
            location={'required': False},
            stars={'required': False}
        ).parse_args()
        h = self.model(id_=args.get('id'), **args).update()
        return self.format_put_response(args, h)
