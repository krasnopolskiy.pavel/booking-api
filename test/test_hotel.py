import os
import unittest
from app import app


class TestHotel(unittest.TestCase):

    def setUp(self):
        self.flask_instance = app
        self.flask_instance.testing = True
        self.client = self.flask_instance.test_client()

    def test_get_single(self):
        id_ = 'ORIYWm3P6WNVcQUeeFq5'
        link = f"/api/hotel?mode=single&id={id_}"
        response = self.client.get(link, headers={"Content-Type": "application/json"}, data={})
        self.assertEqual(200, response.status_code)
        self.assertEqual(id_, response.json['result']['data']['id'])

    def test_get_multiple(self):
        id_list = {'oBpKKf0a1PR8F4JKKKvy', 'lXl0uNpdQ4F9DtG53tTZ'}
        link = f"/api/hotel?mode=multiple&id={','.join(id_list)}"
        response = self.client.get(link, headers={"Content-Type": "application/json"}, data={})
        result_id_set = {h['id'] for h in response.json['result']['data']}
        self.assertEqual(200, response.status_code)
        self.assertEqual(result_id_set, id_list)

    def test_get_query(self):
        id_list = {'oHspV3e5C01BH1ddhTAb', 'lXl0uNpdQ4F9DtG53tTZ'}
        link = f"/api/hotel?mode=query&id={','.join(id_list)}"
        response = self.client.get(link, headers={"Content-Type": "application/json"}, data={})
        self.assertEqual(501, response.status_code)

    def test_put(self):
        id_ = 'oBpKKf0a1PR8F4JKKKvy'
        link = f"/api/hotel?name=changedName&id={id_}"
        response = self.client.put(link, headers={"Content-Type": "application/json"}, data={})
        self.assertEqual(200, response.status_code)
        self.assertEqual(id_, response.json['request']['params']['id'])
        self.assertEqual('OK', response.json['result']['status'])

    def test_post_delete(self):
        link = '/api/hotel?name=TestHotel&city=city/BxsKRTK2HqQOVkl1Eyqr&address=Test St 19&location=12.684,178.685&stars=0'
        response = self.client.post(link, headers={"Content-Type": "application/json"}, data={})
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.json['result']['status'], "OK")
        new_hotel_item = response.json['result']['id']

        link = f'/api/hotel?id={new_hotel_item}'
        response = self.client.delete(link, headers={"Content-Type": "application/json"}, data={})
        self.assertEqual(200, response.status_code)
        self.assertEqual(response.json['result']['status'], "OK")


if __name__ == '__main__':
    unittest.main()
