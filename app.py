from flask import Flask
from flask_restful import Api

from src.booking import BookingResource
from src.hotel import HotelResource
from src.room import RoomResource

app = Flask('BookingApp')
api = Api(app)

api.add_resource(HotelResource, '/api/hotel', '/api/hotel/<string:id>')
api.add_resource(RoomResource, '/api/room',
                 '/api/hotel/<string:id_hotel>/room',
                 '/api/hotel/<string:id_hotel>/room/<string:id>'
                 )
api.add_resource(BookingResource, '/api/booking',
                 '/api/hotel/<string:id_hotel>/room/<string:id_room>/booking'
                 )

if __name__ == '__main__':
    app.run(debug=False, port=5000, host='172.31.87.9')
